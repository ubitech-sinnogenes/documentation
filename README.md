# Data Space Suite Documentation

Note: We are currently in the process of migrating to more recent versions of EDC and EDC UI. Due to this, in the next versions, the API is subject to change.

## Index

- [1. Introduction](./docs/01_introduction.md)
- [2. Architecture](./docs/02_architecture.md)
- [3. Registration](./docs/03_registration.md)
- [4. Installation](./docs/04_installation.md)
- [5. Usage](./docs/05_usage.md)
- [6. Troubleshooting](./docs/06_troubleshooting.md)
- [7. Resources](./docs/07_resources.md)

## Generate a PDF of the documentation

Requirements: Docker

To generate a PDF document combining all markdown files in the `docs` directory:

```
./generate-pdf.sh
```

A `documentation.pdf` should be created in the root directory of this project.

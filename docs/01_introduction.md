# Introduction

The Connector is a piece of software used within the data space for trusted data exchange.
Each data provider or data consumer should install their own instance of the connector.

In every instance of the Connector, the following services are managed by Docker:

- Connector Core

    The back-end of the connector.

- Connector UI

    The front-end of the connector.

- Connector Database

    A PostgreSQL Database which stores data assets, contracts, usage policies created through the Connector.

- Connector File Server

    An HTTP server responsible for providing the Connector with a data source endpoint and a data sink endpoint to facilitate file transfers. It also provides a list of incoming or outgoing files to the Connector UI and an endpoint to download incoming files.

- Connector Auth Proxy

    An HTTP server responsible for protecting the Connector Management API. All requests to the Management API are configured to go through this proxy which first validates the request Authorization header and then forwards the request.

Connector services managed by Docker:

![Connector services managed by Docker](images/docker.png)

Data transfer using the bundled file server:

![Data transfer using the bundled file server](images/file-transfer.png)

Calling the management API through the auth proxy:

![Calling the management API through the auth proxy](images/auth-proxy.png)

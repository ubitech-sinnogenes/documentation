# Architecture

Please refer to:

- [Our presentation (slides 12-23)](https://ubitecheu.sharepoint.com/:p:/r/sites/SINNOGENES/_layouts/15/Doc.aspx?sourcedoc=%7BDF05EED6-6C06-496D-B0F1-B82BC967C22F%7D&file=WP2%20Monthly%20Meeting%20Agenda%208_9_23.pptx&action=edit&mobileredirect=true) (slightly outdated) from the SINNOGENES WP2 Monthly Meeting on 08-09-2023.
- [Our workshop presentation](https://ubitecheu.sharepoint.com/:p:/r/sites/SINNOGENES/_layouts/15/Doc.aspx?sourcedoc=%7BE411DA4B-6EC3-482E-9E9F-0B36B55AA980%7D&file=SINNOGENES-Connector_Workshop.pptx&action=edit&mobileredirect=true) from the T2.3 & T2.4 Workshop on 03-11-2023.


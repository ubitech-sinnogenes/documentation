# Troubleshooting

## Logs
If anything goes wrong during the transfer process, you may check the connector logs.

For example, to check the Connector Core logs:
```
docker logs --follow connector-core
```

## Known issues

### Wrong transfer state in the Connector UI

We are currently investigating this issue. It is possibly [an upstream bug](https://github.com/sovity/edc-ui/discussions/514).
